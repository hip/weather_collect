# Weather Collect
Uses https://github.com/bytesnz/vproweather to interface with Davis Vantage.

1. `git submodule update --init --recursive` - installs vproweather package from github (submodule).
2. `cd` into ~/weather_interface/vproweather and use the `make` command to build the program.
3. Create a crontab entry that automatically runs the collect script at a set interval. Example: `*/5 * * * * cd /home/debian/weather_interface; ./collect;`.
4. Export data. The easiest way to do this is to set up a cron script to use the `scp` command to transfer files to the system where the weather interface is running.